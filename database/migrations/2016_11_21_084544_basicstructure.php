<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Basicstructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menius', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });

        Schema::create('dishes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('photo');
            $table->text('description');
            $table->decimal('netto_price', 8, 2);
            $table->decimal('price', 8, 2);
            $table->integer('quantity');
            $table->integer('meniu_id')->unsigned();
            $table->timestamps();

            $table->foreign('meniu_id')->references('id')->on('menius');
        });

        Schema::create('tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('number_of_person');
            $table->timestamps();
        });

        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            
        });

        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('longitude', 8, 3);
            $table->float('latitude', 8, 3);
            $table->text('working_hours');
            $table->text('info');
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('table_id')->unsigned();
            $table->integer('number_of_people');
            $table->string('contact_phone');
            $table->datetime('reservation_date');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');

            $table->foreign('table_id')->references('id')->on('tables');

        });

        Schema::create('orderitems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->decimal('price', 8, 2);
            $table->decimal('netto_price', 8, 2);
            $table->integer('quantity');
            $table->integer('order_id')->unsigned();
            $table->integer('dish_id')->unsigned();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('dish_id')->references('id')->on('dishes');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menius');
        Schema::drop('dishes');
        Schema::drop('tables');
        Schema::drop('carts');
        Schema::drop('contacts');
        Schema::drop('orders');
        Schema::drop('orderitems');
    }
}
