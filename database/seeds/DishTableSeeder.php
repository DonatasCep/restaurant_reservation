<?php

use Illuminate\Database\Seeder;

class DishTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('dishes')->insert([
        	
        	'title'=> 'Cepelinai',
        	'photo'=> 'llll',
        	'description'=> 'skanu',
        	'netto_price'=> 12,
        	'price'=> 24,
        	'quantity'=> 1,
        	'meniu_id'=> 1
        ]); 

    }
}
