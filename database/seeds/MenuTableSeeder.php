<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menius')->insert([
            'title' => 'Day Menu',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        DB::table('menius')->insert([
            'title' => 'Chritsmas Menu',
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
