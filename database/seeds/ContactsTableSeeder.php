<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
      	DB::table('contacts')->insert([
        'name' => 'Kontaktai',
        'latitude' => -25.363,
        'longitude' => 131.044,
        'working_hours' => 'Darbo laikas
        I-V 11:00 - 20:00
        VI  10:00 - 03:00
        VII 10:00 - 03:00',
        'info' => 'Papildoma informacija',
        'created_at' => new \DateTime(),
        'updated_at' => new \DateTime()

      	]);
    }
}

