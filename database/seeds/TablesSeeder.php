<?php
use App\Table;
use Illuminate\Database\Seeder;

class TablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	for($i = 1; $i < 11; $i++) {
        	
        	Table::create([
        		'number_of_people' 	=> rand(2, 8),
        		'name'				=> 'Table no. ' . $i,
        	]);

    	}
    }
}
