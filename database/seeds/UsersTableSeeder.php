<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name'=> 'Petras',
        	'surname'=> 'Petraitis',
        	'birthdate'=> '1980-02-15',
        	'phone_number'=> 862358625,
        	'email'=> 'petras@gmail.com',
        	'password'=> bcrypt('secret'),
        	'address'=> 'Svitrigailos 8',
        	'city'=> 'Vilnius',
        	'zip_code'=> '235625',
        	'country'=> 'Lithuania'
       	]);    
    	
    }	
}
