function removeCartItem(){
	var id = $(this).data('dish-id');

	$.ajax({
		method: 'POST',
		url: CART_DESTROY_URL,
		data: {
			dish_id: id,
			_token: CSRF_TOKEN
		},
		success: function(response){
			$('.cart-total-items').html(response.total_items);
			$('.cart-total-amount').html(response.total_amount);

			$('.dropdown-cart').html(' ');
			$('.cart-table').html(' ');


			for(var i = 0; i < response.items.length; i++){

					// cia turi buti visos klases kaip ir show page
					var item = '<li>' +
					'<span class="item">' + 
					'<span class="item-left">' +
					'<span class="item-info">' +
					'<a href="#">' +
					response.items[i].title +  'x' + response.items[i].cart_count +
					'</a>' +
					'</span>' +
					'</span>' +
					'<span class="item-right">' +
					'<a href="#" class="btn btn-xs btn-danger pull-right cart-destroy" data-dish-id="'+response.items[i].id+'">x</a>' +
					'</span>' +
					'</span></li>';

					$('.dropdown-cart').append(item);

					item = '<hr>' +
					'<div class="row">' + 
					'<div class="col-xs-2">' +
					'<img class="img-responsive" src="'+ response.items[i].photo + '">' +
					'</div>' +
					'<div class="col-xs-4">' +
					'<h4 class="product-name"><strong>' +
					response.items[i].title +
					'</strong></h4>' +
					'<h4><small>' +
					response.items[i].description +
					'</small></h4>' +
					'</div>' +
					'<div class="col-xs-6">' +
					'<div class="col-xs-6 text-right">' +
					'<h6><strong>' + response.items[i].price +
					'<span class="text-muted"> x </span></strong></h6>' +
					'</div>' +
					'<div class="col-xs-4">' +
					'<input type="text" class="form-control input-sm" value="' + response.items[i].cart_count + '">' +
					'</div>' +
					'<div class="col-xs-2">' +
					'<a href="#" class="btn btn-link btn-xs cart-destroy" data-dish-id="'+response.items[i].id+'">' +
					'<span class="glyphicon glyphicon-trash"> </span></a>' +
					'</div>' +
					'<div class="col-xs-2">' +
					'<h6><strong><span class="text-muted">' + response.items[i].cart_amount + '</span> €</strong></h6>"' +
					'</div>' +
					'</div>' +
					'</div>';
					

					$('.cart-table').append(item);
					
					
				}
				$('.cart-destroy').click(removeCartItem);
			}
		});
}


$(document).ready(function() {
	$('.cart-destroy').click(removeCartItem);

	$('.cart-destroy-all').click(function() {
		$.ajax({
			method: "POST",
			url: CART_DESTROY_ALL_URL,
			data: {
				_token: CSRF_TOKEN
			},
			success: function(response) {
				// Užklausa sėkminga
				$('.cart-total-items').html(0);
				$('.cart-total-amount').html(0);

				$('.dropdown-cart').html(" ");

			},
			fail: function(error) {
				// Užklausa nepavyko
				console.log('Fail');
				console.log(error);
			}
		});
	});

	$('.add-to-cart').click(function() {
		var id = $(this).data('dish-id');
		
		$.ajax({
			method: "POST",
			url: CART_UPDATE_URL,
			data: {
				dish_id: id,
				_token: CSRF_TOKEN
			},
			success: function(response) {
				// Užklausa sėkminga
				$('.cart-total-items').html(response.total_items);
				$('.cart-total-amount').html(response.total_amount);

				$('.dropdown-cart').html(' ');
				
				for(var i = 0; i < response.items.length; i++){

					// cia turi buti visos klases kaip ir show page
					var item = '<li>' +
					  	'<span class="item">' + 
					  	'<span class="item-left">' +
					  	'<span class="item-info">' +
					  	'<a href="#">' +
						response.items[i].title + 'x' + response.items[i].cart_count +
						'</a>' +
						'</span>' +
						'</span>' +
						'<span class="item-right">' +
						'<a href="#" data-dish-id="' + response.items[i].id +
						'" class="btn btn-xs btn-danger pull-right cart-destroy">x</a>' +
						'</span>' +
						'</span></li>';

						$('.dropdown-cart').append(item);

				}
				$('.cart-destroy').click(removeCartItem);
			}
		});		
	});
});

