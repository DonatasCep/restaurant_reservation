<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = [
    	'name',
        'number_of_person'
        
    ];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    function getNameWithCapacityAttribute() {
    	
    	return $this->name . ' (' . $this->number_of_person . ' persons)';

    }
}
