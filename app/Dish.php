<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $fillable = [
    	'title',
    	'photo',
    	'description',
    	'netto_price',
    	'price',
    	'quantity',
        'meniu_id'
    ];

    public function meniu() {

    	return $this->belongsTo('App\Meniu');
    }

    public function carts() {

    	return $this->belongsToMany('App\Cart');
    }

    public function orders() {

    	return $this->hasManyThrough('App\Order', 'App\OrderItem');
    }
}
