<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meniu extends Model
{
    public function dishes() {

    	return $this->hasMany('App\Dish');
    }
}
