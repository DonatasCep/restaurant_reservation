<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'birthdate',
        'phone_number',
        'email', 
        'password',
        'address',
        'city',
        'zip_code',
        'country',
        'is_admin'
    ];

     public function cart()
    {
        return $this->hasOne('App\Cart');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
