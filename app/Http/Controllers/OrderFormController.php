<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\Table;
use Carbon\Carbon;

class OrderFormController extends Controller
{
    
    function __construct() 
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('orderform.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $cartItems = session()->get('cart.items');
        if(!count($cartItems)) {
            return redirect()->action('MeniuController@index');
        }

        $user = Auth::user();
        $tables = Table::all();
        // su pluck pasiemam is duombazes informacija
        $tables = $tables->pluck('NameWithCapacity', 'id');


        // //ar prisijunges ar ne cia padarem su middleware
        // if (Auth::guest()) {
        //   return  redirect(url('login'));
        // }    
        //jeigu neprisijunges


        return view('orderform.create', compact('user', 'tables'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $this->validate($request, [
        'table_id' =>           'required|exists:tables,id|integer',
        'contact_phone' =>      'required|min:8|max:12',
        'number_of_person' =>   'required|min:1|integer',
        'reservation_date' =>   'required|date_format:Y-m-d H:i',

        ]);

        // grazinti vartotoja kai netinka su zmoniu skaiciu prie stalo

        $capacity = Table::find($request->table_id)->number_of_person;

        $number_of_person = $request->number_of_person;

        if ($capacity < $number_of_person) {
            
            return  back()
                ->withErrors('To much people selected')
                ->withInput();
            
        }

        // grazinti vartotojui klaida kai jau uzimta data


        $before = new Carbon($request->reservation_date);
        $before = $before->subHours(1);

        $after = new Carbon($request->reservation_date);
        $after = $after->addHours(1);

        $orders = Order::whereBetween('reservation_date', [$before, $after])
            ->where('table_id', $request->table_id)
            ->get();

        if (count($orders)) {
            return back()
                ->withErrors('This time is capture at this time')
                ->withInput();
        }

        // Surinkti duomenis is formos

        $data = $request->all();

        // issaugoti orderi duomenu bazeje
        $user = Auth::user();

        $order = $user->orders()->create($data);

        // dd(session()->get('cart.items'));
        $cartItems = session()->get('cart.items');

        foreach($cartItems as $item){
            $item['dish_id'] = $item['id'];    
            $order->order_items()->create($item);

        }

        $request->session()->forget('cart.items');
        $request->session()->put('cart.total_items', 0);
        $request->session()->put('cart.total_amount', 0);


        // peradrisuoti vartotoja i sekmes puslapi arba paskutini stepa
        return redirect()->route('orderform.show', $order->id);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // gauti irasa su nuoroda id
        $order = Order::find($id);
        // atspausdinti sekmes puslapi ir informacija apie uzsakyma (order)
        return view('orderform.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
