<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DishController extends Controller
{   
    public function __construct ()
    {

        // $this->middleware('auth', ['only' => 'create']);
        $this->middleware('auth', ['except' => ['index', 'show']]);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishes = \App\Dish::all();

        return view('dishes.index', compact('dishes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   

        $menius = \App\Meniu::all();

        $selectMenus = array();

        foreach ($menius as $menu) {
            $selectMenus[$menu->id] = $menu->title;
        }
        return view('dishes.create', compact('selectMenus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = \App\Dish::create($request->all());
        return redirect()->route('meniu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

        $dishes = \App\Dish::find($id);

        return view('dishes.show',compact('dishes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dish = \App\Dish::find($id);
        $menius = \App\Meniu::all();

        $selectMenus = array();

        foreach ($menius as $menu) {
            $selectMenus[$menu->id] = $menu->title;
        }

        return view('dishes.create', compact('dish','selectMenus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \App\Dish::find($id)->update($request->all());

        return redirect()->route('meniu.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Dish::find($id)->delete();

        return redirect()->route('meniu.index');
    }
}
