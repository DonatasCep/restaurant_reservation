<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $items = $request->session()->get('cart.items');
        //     if (!count($items)) {
        //         return redirect()->action('MeniuController@index');
        //     }

        return view('cart.index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    public function store(Request $request)
    {        

        $total_items = 0;
        $total_amount = 0;
        $cart_amount = 0;
        $items = [];

        // Surandam pridedamą patiekalą DB
        $id = $request->dish_id;
        $dish = \App\Dish::find($id)->toArray();
        $dish['cart_count'] = 1;
        // $dish['cart_amount'] = $dish->price;

        $items = $request->session()->get('cart.items');

        if($items) {

            // cia tikrinam ar yra tas pats patiekalas ir jei yra prideda prie to pacio
            $found = false;
            for($i = 0; $i < count($items); $i++){
                if($items[$i]['id'] == $dish['id']){
                    $items[$i]['cart_count']++;
                    $found = true;
                    break;
                }
            }

            // cia randa jei nera to pacio patiekalo prideda nauja
            if(!$found){
                $items[] = $dish;
            }

            for($i = 0; $i < count($items); $i++){
                $items[$i]['cart_amount'] = $items[$i]['cart_count'] * $items[$i]['price'];
                $total_amount += $items[$i]['cart_amount'];
                $total_items += $items[$i]['cart_count'];
            }

        } else {
            $items[] = $dish;
            $total_amount = $dish['price'];
            $total_items = 1;
        }

        $request->session()->put('cart.items', $items);
        $request->session()->put('cart.total_items', $total_items);
        $request->session()->put('cart.total_amount', $total_amount);
       
        // Atiduodam rezultatą
        $result = [
            "total_items"   => $total_items,
            "total_amount"  => $total_amount,
            "items"         => $items
        ];

        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    
    }

    public function destroyAll(Request $request)
    {   
        $request->session()->forget('cart.items');
        $request->session()->put('cart.total_items', 0);
        $request->session()->put('cart.total_amount', 0);

        return;     
    }

    public function destroy(Request $request)
    {
       
        $total_items = 0;
        $total_amount = 0;
        $items = [];

        // Surandam pridedamą patiekalą DB
        $id = $request->dish_id;

        $items = $request->session()->get('cart.items');

            
        
        for($i = 0; $i < count($items); $i++){
            if($items[$i]['id'] == $id){
                array_splice($items, $i, 1);
                break;
            }
        }

        for($i = 0; $i < count($items); $i++){
            $total_amount += $items[$i]['cart_count'] * $items[$i]['price'];
            $total_items += $items[$i]['cart_count'];
        }


        $request->session()->put('cart.items', $items);
        $request->session()->put('cart.total_items', $total_items);
        $request->session()->put('cart.total_amount', $total_amount);
       
        // Atiduodam rezultatą
        $result = [
            "total_items"   => $total_items,
            "total_amount"  => $total_amount,
            "items"         => $items
        ];

        return $result;
    }
}