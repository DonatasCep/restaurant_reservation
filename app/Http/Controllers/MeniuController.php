<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;


class MeniuController extends Controller
{   

    public function __construct ()
    {

        // $this->middleware('auth', ['only' => 'create']);
        $this->middleware('auth', ['except' => ['index', 'show']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menius = \App\Meniu::all();
        foreach ($menius as $meniu) {
            $meniu["dishes"] = \App\Dish::where('meniu_id', $meniu->id)->limit(3)->get();
        }
        

        return view('meniu.index', compact('menius'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = \App\Meniu::create($request->all());
        return redirect()->route('meniu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   //gaunamm visus meniu irasus
        $meniu = \App\Meniu::find($id);

        // gautu patiekalus pagal kategorija
        $dishes = \App\Dish::where('meniu_id', $id)->get();
        // atvaizduojam
        return view('meniu.show', compact('meniu', 'dishes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
