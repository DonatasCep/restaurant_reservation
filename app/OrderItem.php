<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'orderitems';
    
    protected $fillable = [
    	'title',
    	'description',
    	'price',
    	'netto_price',
    	'quantity',
        'order_id',
        'dish_id'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function dish()
    {
        return $this->hasOne('App\Dish');
    }
}
