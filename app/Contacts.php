<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $fillable = [
        'latitude',
        'longitude',
        'name',
        'working_hours',
        'info'
    ];
}
