<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/contacts', function() {
  $id = 1;
  $contacts = \App\Contacts::find($id);

  return view('contacts.index', compact('contacts'));

});

Route::get('/', 'MeniuController@index');

Route::resource('dishes', 'DishController');
// Route::resource('/meniu', 'MeniuController@index');
Route::resource('user', 'UserController');
Route::resource('cart', 'CartController');
Route::resource('meniu', 'MeniuController');
Route::resource('orderform', 'OrderFormController');
Route::resource('payment', 'PaymentController');
Route::resource('checkout', 'CheckoutController');


Route::post('cart', 'CartController@store')->name('cart.store');
Route::post('cartDestroyAll', 'CartController@destroyAll')->name('cart.destroyAll');
Route::post('cartDestroy', 'CartController@destroy')->name('cart.destroy');








