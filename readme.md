# Restaurant reservation site #

This site was created using:

* Laravel framework; 
* HTML;
* CSS;
* PHP; 
* MySql database;

### Site has this elements ###

* Connection with MySql Database
* Meniu List
* Can create admin and users

### How set up it? ###

* Inside Mysql create database reservation_db also db is main folder
* run command in yuor command prompt "git clone https://DonatasCep@bitbucket.org/DonatasCep/restaurant_reservation.git"
* run command "composer install"
* rename ".env.example" file in main directory to ".env"
* run "php artisan key:generate"
* open ".env" file, change DB_DATABASE=homestead DB_USERNAME=homestead
	DB_PASSWORD=secret to DB_DATABASE=reservation_db DB_USERNAME=root DB_PASSWORD=
* run command "php artisan migrate"
* run command "php artisan db:seed --class=UserTableSeeder"
* run command "php artisan db:seed --class=TasksTableSeeder"