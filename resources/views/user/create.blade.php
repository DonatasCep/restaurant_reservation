@extends('main')

@section('content')

<h1>User info page</h1>


	
		
	
@if(isset($user))
{{ Form::open(['route' => ['user.update', $user->id], "method" => "POST"]) }}
	<input type="hidden" name="_method" value="PUT">
@else

{!! Form::open(['url' => '/user', 'id'=>'myRegister', 'method' => "POST"]) !!}
@endif
	{{ Form::label('Name') }}<br>
	@if(isset($user))
		{{ Form::text('name', $user->name, ['class' => 'form-control']) }}
	@else
		{{ Form::text('name','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Surname') }}<br>
	@if(isset($user))
		{{ Form::text('surname', $user->surname, ['class' => 'form-control']) }}
	@else
		{{ Form::text('surname','',['class' => 'form-control']) }}<br>
	@endif
	
	{{ Form::label('Birthdate') }}<br>
	@if(isset($user))
		{{ Form::date('birthdate', $user->birthdate, ['class' => 'form-control']) }}
	@else
		{{ Form::date('birthdate','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Phone Number') }}<br>
	@if(isset($user))
		{{ Form::text('phone_number', $user->phone_number, ['class' => 'form-control']) }}
	@else
		{{ Form::text('phone_number','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Email') }}<br>
	@if(isset($user))
		{{ Form::text('email', $user->email, ['class' => 'form-control']) }}
	@else
		{{ Form::text('email','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Address') }}<br>
	@if(isset($user))
		{{ Form::text('address', $user->address, ['class' => 'form-control']) }}
	@else
		{{ Form::text('address','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('City') }}<br>
	@if(isset($user))
		{{ Form::text('city', $user->city, ['class' => 'form-control']) }}
	@else
		{{ Form::text('city','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Zip Code') }}<br>
	@if(isset($user))
		{{ Form::text('zip_code', $user->zip_code, ['class' => 'form-control']) }}
	@else
		{{ Form::text('zip_code','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Country') }}<br>
	@if(isset($user))
		{{ Form::text('country', $user->country, ['class' => 'form-control']) }}
	@else
		{{ Form::text('country','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Password') }}<br>
	@if(isset($user))
		{{ Form::text('password','', ['class' => 'form-control']) }}
	@else
		{{ Form::text('password','',['class' => 'form-control']) }}<br>
	@endif

	@if($user->is_admin == 1)
		<input type="hidden" name="is_admin" value="0">
		<input type="checkbox" checked="" name="is_admin" value="1">Admin<br>
	@else
		<input type="hidden" name="is_admin" value="0">
		<input type="checkbox" name="is_admin" value="1">Admin<br>
	@endif

	{!! Form::submit('Submit', ['class' => 'btn btn-warning']); !!}
	{!! Form::close() !!}


@if(isset($user))

{{ Form::open(['route' => ['user.destroy', $user->id], "method" => "POST"]) }}
	<input type="hidden" name="_method" value="DELETE">

	{{ csrf_field() }}

	
	{!! Form::submit('Detele', ['class' => 'btn btn-warning']); !!}
	{!! Form::close() !!}

@endif




@endsection