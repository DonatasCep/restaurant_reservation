@extends('main')

@section('content')

<h3>Users info</h3>
	
<table>
	<tr>
		<th>Name</th>
		<th>Surname</th>
		<th>Birthdate</th>
		<th>Phone Number</th>
		<th>Email</th>
		<th>Address</th>
		<th>City</th>
		<th>Zip Code</th>
		<th>Country</th>
		<th>Admin</th>
		<th></th>
	</tr>
	@foreach($users as $user)
	<tr>
		<td>{{ $user->name }}</td>
		<td>{{ $user->surname }}</td>
		<td>{{ $user->birthdate }}</td>
		<td>{{ $user->phone_number }}</td>
		<td>{{ $user->email }}</td>
		<td>{{ $user->address }}</td>
		<td>{{ $user->city }}</td>
		<td>{{ $user->zip_code }}</td>
		<td>{{ $user->country }}</td>
		<td>{{ $user->is_admin }}</td>
		@if(Auth::guest())

		@else
		<td><a href="{{ route('user.edit', $user->id) }}" class="btn btn-success">Edit</a></td>
		@endif
	</tr>
	@endforeach 
</table>


@endsection