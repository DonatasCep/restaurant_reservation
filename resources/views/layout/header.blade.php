<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Restaurant</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	<!-- Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sofia|Dancing+Script" rel="stylesheet" type="text/css">
    <script>
        var CART_UPDATE_URL = "{{ route('cart.store') }}";
        var CART_DESTROY_ALL_URL = "{{ route('cart.destroyAll') }}";
        var CART_DESTROY_URL = "{{ route('cart.destroy') }}";
        var CSRF_TOKEN = "{{ csrf_token() }}";
    </script>
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a class="navbar-brand" href="{{ url('/meniu') }}">
                        <img class="img-responsive logo" src="{{ asset('img/logo1.png') }}" alt=""/></a>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a class="btn-deflaut dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Our Meniu<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="{{ url('/meniu') }}">Home Page</a></li>
                                <li><a href="{{ url('/meniu/1') }}">Day Meniu</a></li>
                                <li><a href="{{ url('/meniu/2') }}">Cristmas Meniu</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="{{ url('/contacts') }}">Contact</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown cart">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-shopping-cart"></span>
                        <span class="cart-total-items">{{ Session::get('cart.total_items') }}</span>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu scrollable-menu" role="menu">
                        <li>
                            <ul class="dropdown-cart">
                            @if(Session::has('cart.items'))
                            @foreach(Session::get('cart.items') as $dish)
                                <li>
                                    <span class="item">
                                        <span class="item-left">
                                            <span class="item-info">
                                                <a href="#">{{$dish['cart_count'] }} x {{ $dish['title'] }}</a>
                                            </span>
                                        </span>
                                        <span class="item-right">
                                            <a href="#" data-dish-id="{{ $dish['id'] }}" class="btn btn-xs btn-default pull-right cart-destroy">x</a>
                                        </span>
                                    </span> 
                                </li>
                            @endforeach
                            @endif 
                            </ul>
                        </li>
                        <li class="divider"></li>
                            <li>
                                <p class="total-amount">Total amount: <span class="cart-total-amount">{{ Session::get('cart.total_amount') }}</span> €</p>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#" class="btn cart-destroy-all" >Clear</a></li>
                            <li class="divider"></li>
                            <li><a class="text-center" href="{{ url('/cart') }}">View Cart</a></li>
                            <li class="divider"></li>
                            <li><a class="text-center" href="{{url('/cart')}}">Checkout</a></li>
                    </ul>
                </li>
                        <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                <li class="dropdown dropdown-login">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ url('/logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Logout</a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
