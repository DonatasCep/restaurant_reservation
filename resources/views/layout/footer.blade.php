	<footer class="footer">
		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p>Copyright &copy; Donatas 2014</p>
            </div>
        </div>
	</footer>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 	<script>
// just for the demos, avoids form submit
	
	$( "#myForm" ).validate({
  		rules: {
    		reservation_date: {
      			required: true,
      			date: true
    		}
  		}
	});
	
		$( "#myRegister" ).validate({
	  		rules: {
	    		email: {
	      			required: true,
	      			email: true
	    		},
	    		address: {
	    			required:true
	    		}
	  		}
		});
	</script>
 	<script type="text/javascript" src="{{ asset('js/javascript.js') }}"></script>
	</body>
</html>
