
@extends('main')

@section('content')

<h1>Day Offer</h1>

<div class="card">
	<div class="container-fliud">
		<div class="wrapper row">
			<div class="preview col-md-6">
				<div class="preview-pic tab-content">
					<img src="{{ $dishes->photo }}" />	
				</div>
			</div>
			<div class="details col-md-6">
				<h3 class="product-title">{{ $dishes->title }}</h3>
				<p class="product-description">{{ $dishes->description }}</p>
				<h4 class="price">current price: <span>{{ $dishes->price }}  €</span></h4>
				<div class="action">
					<button data-dish-id="{{ $dishes->id }}" 
					 class="add-to-cart btn btn-default" type="button">Add to cart
					</button>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection