@extends('main')

@section('content')



@if(isset($dish))
{{ Form::open(['route' => ['dishes.update', $dish->id], "method" => "POST"]) }}
	<input type="hidden" name="_method" value="PUT">


@else

{!! Form::open(['url' => '/dishes', 'method' => "POST"]) !!}
@endif
	
	<h1>Add Dish</h1>

	{{ Form::label('Title') }}<br>
	@if(isset($dish))
		{{ Form::text('title', $dish->title, ['class' => 'form-control']) }}
	@else
		{{ Form::text('title','',['class' => 'form-control']) }}<br>
	@endif
	
	{{ Form::label('Image') }}<br>
	@if(isset($dish))
	{{ Form::textarea('photo', $dish->photo, ['class' => 'form-control']) }}
 	@else
	{{ Form::textarea('photo', '',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Description') }}<br>
	@if(isset($dish))
	{{ Form::text('description', $dish->description, ['class' => 'form-control'] ) }}
	@else
	{{ Form::text('description', '',['class' => 'form-control']) }}<br>
	@endif
	
	{{ Form::label('Netto Price') }}<br>
	@if(isset($dish))
		{{ Form::number('netto_price', $dish->netto_price, ['class' => 'form-control']) }}
	@else
		{{ Form::number('netto_price','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Price') }}<br>
	@if(isset($dish))
		{{ Form::number('price', $dish->price, ['class' => 'form-control']) }}
	@else
		{{ Form::number('price','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Quantity') }}<br>
	@if(isset($dish))
	{{ Form::text('quantity', $dish->quantity, ['class' => 'form-control'] ) }}
	@else
	{{ Form::text('quantity', '',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Meniu_id') }}<br>
	@if(isset($dish))
		{{ Form::select('meniu_id', $selectMenus, $dish->meniu_id, ['class' => 'form-control']) }}<br>
	@else
		{{ Form::select('meniu_id', $selectMenus, ['class' => 'form-control']) }}
	@endif

	{!! Form::submit('Save', ['class' => 'btn btn-warning btn-saveDelete']); !!}
{!! Form::close() !!}

@if(isset($dish))

{{ Form::open(['route' => ['dishes.destroy', $dish->id], "method" => "POST"]) }}
	<input type="hidden" name="_method" value="DELETE">

	{{ csrf_field() }}

	
	{!! Form::submit('Detele', ['class' => 'btn btn-warning btn-saveDelete']); !!}
	{!! Form::close() !!}

@endif

@endsection