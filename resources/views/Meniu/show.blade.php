
@extends('main')

@section('content')

<h1>Meniu</h1>

<div class="row">

@foreach($dishes as $index => $dish)
<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="thumbnail">
       <a href="{{ route('dishes.show', $dish->id) }}"><img src="{{ $dish->photo }}" alt=""></a>
       <div class="caption">
            <h4><a href="{{ route('dishes.show', $dish->id) }}">{{ $dish->title }}</a></h4>
            <p>{{$dish->description}}</p>
            <h4 class="pull-right">{{$dish->price}}</h4> 
        </div>
    <a href="{{ route('dishes.edit', $dish->id) }}" width="200px" class="btn btn-success btn-edit">EDIT</a> 
    @if(++$index % 4 == 0)
        </div><div class="row">
    @endif
    </div>
</div>
@endforeach
    
</div>


@endsection