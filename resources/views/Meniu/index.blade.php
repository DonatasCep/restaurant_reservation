@extends('main')

@section('content')
<div class="row">
	<div class="col-md-12">
        <h1>Le Bon Crubeen</h1>
		<div class="row carousel-holder">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img class="slide-image" src="{{ asset('img/slider1.jpg') }}" alt="">
                    </div>
                    <div class="item">
                        <img class="slide-image" src="{{ asset('img/slider2.jpg') }}" alt="">
                    </div>
                    <div class="item">
                        <img class="slide-image" src="{{ asset('img/slider3.jpg') }}" alt="">
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
      </div>
</div>
<a href="{{ route('dishes.create') }}" class="btn btn-info">Add dish</a>
<a href="{{ route('user.index') }}" class="btn btn-info">View users</a>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
            @foreach ($menius as $index => $menu)
            <h3>
                <a href="{{ route('meniu.show', $menu->id) }}">{{ $menu->title }}</a>
            </h3>
                @foreach($menu["dishes"] as $dish)
                
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="thumbnail">
                     <a href="{{ route('dishes.show', $dish->id) }}"><img src="{{ $dish->photo }}" alt=""></a>
                     <div class="caption">
                        <h4 class="pull-right">{{$dish->price}}  €</h4>
                        <h4><a href="{{ route('dishes.show', $dish->id) }}">{{ $dish->title }}</a></h4>
                        <p>{{substr($dish->description, 0, 50)}}</p>
                    </div>
                    <a href="{{ route('dishes.edit', $dish->id) }}" width="200px" class="btn btn-success btn-edit">EDIT</a>
                    <a  data-dish-id="{{ $dish->id }}" name="stay_here" href="#stay_here" width="100px" class="btn btn-success add-to-cart btn-edit">Add to cart</a>    
                    </div>
                </div>

                @if(++$index % 4 == 0)
                    </div><div class="row">
                @endif

                @endforeach
        @endforeach
    </div>
</div>
  
@endsection