@extends('main')

@section('content')

<h2>Payment page</h2>

<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <a type="button" class="btn btn-danger btn-circle" href="{{ url('/cart') }}">1</a>
            <p>Cart</p>
        </div>
        <div class="stepwizard-step">
            <a type="button" class="btn btn-primary btn-circle" href="{{ url('/orderform') }}">2</a>
            <p>Order Form</p>
        </div>
        <div class="stepwizard-step">
            <a type="button" class="btn btn-success btn-circle" href="{{ url('/payment') }}">3</a>
            <p>Payment</p>
        </div> 
    </div>
</div>

<table class="table table-striped table-hover table-bordered">
    <tbody>
        <tr>
            <th>Item</th>
            <th>QTY</th>
            <th>Unit Price</th>
            <th>Total Price</th>
        </tr>
        @if(Session::has('cart.items'))
        @foreach(Session::get('cart.items') as $dish)
            <tr>
                <td>{{ $dish['title'] }}</td>
                <td>{{ $dish['cart_count'] }}</td>
                <td>{{ $dish['price'] }} €</td>
                <td><span>{{ $dish['cart_amount'] }} €<span></td>
            </tr>
        @endforeach
        @endif
            <tr>
                <th colspan="3"><span class="pull-right cart-total-amount">Sub Total</span></th>
                <th>{{ Session::get('cart.total_amount') }} €</th>
            </tr>
            <tr>
                <th colspan="3"><span class="pull-right">VAT 21%</span></th>
                <th>£50.00</th>
            </tr>
            <tr>
                <th colspan="3"><span class="pull-right">Total</span></th>
                <th>£300.00</th>
            </tr>
        <tr>
            <td><a href="{{ url('/table') }}" class="btn btn-primary">Back</a></td>
            <td colspan="3"><a href="#" class="pull-right btn btn-success">Pay</a></td>
        </tr>
    </tbody>
</table>



<form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="float:right">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="yourPayPalAccount@email.lt">
    <input type="hidden" name="currency_code" value="EUR">
    <input type="hidden" name="item_name" value="Invoice">
    <input type="hidden" name="amount" value="12.99">
    <input type="image" src="http://www.paypal.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="Make payments with PayPal - it\'s fast, free and secure!">
</form>






@endsection