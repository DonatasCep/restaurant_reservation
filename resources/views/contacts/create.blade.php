@extends('main')

@section('content')

@if(isset($contacts))
{{ Form::open(['route' => ['contacts.update', $contacts->id], "method" => "POST"]) }}
	<input type="hidden" name="_method" value="PUT">


@else

{!! Form::open(['url' => '/contacts', 'method' => "POST"]) !!}
@endif
	
	<h1>Edit Contacts</h1>

	{{ Form::label('Name') }}<br>
	@if(isset($contacts))
		{{ Form::text('name', $contacts->name, ['class' => 'form-control']) }}
	@else
		{{ Form::text('name','',['class' => 'form-control']) }}<br>
	@endif
	
	{{ Form::label('Longitude') }}<br>
	@if(isset($contacts))
	{{ Form::textarea('longitude', $contacts->longitude, ['class' => 'form-control']) }}
 	@else
	{{ Form::textarea('longitude', '',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Description') }}<br>
	@if(isset($dish))
	{{ Form::text('description', $dish->description, ['class' => 'form-control'] ) }}
	@else
	{{ Form::text('description', '',['class' => 'form-control']) }}<br>
	@endif
	
	{{ Form::label('Netto Price') }}<br>
	@if(isset($dish))
		{{ Form::number('netto_price', $dish->netto_price, ['class' => 'form-control']) }}
	@else
		{{ Form::number('netto_price','',['class' => 'form-control']) }}<br>
	@endif

	{{ Form::label('Price') }}<br>
	@if(isset($dish))
		{{ Form::number('price', $dish->price, ['class' => 'form-control']) }}
	@else
		{{ Form::number('price','',['class' => 'form-control']) }}<br>
	@endif

	{!! Form::submit('Edit', ['class' => 'btn btn-warning']); !!}
{!! Form::close() !!}

{{ Form::open(['route' => ['dishes.destroy', $dish->id], "method" => "POST"]) }}
	<input type="hidden" name="_method" value="DELETE">

	{{ csrf_field() }}

	
	{!! Form::submit('Detele', ['class' => 'btn btn-warning']); !!}
	{!! Form::close() !!}

@endif


@endsection