@extends('main')

@section('content')

<h1>Order Form</h1>

<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <a type="button" class="btn btn-danger btn-circle" href="{{ url('/cart') }}">1</a>
            <p>Cart</p>
        </div>
        <div class="stepwizard-step">
            <a type="button" class="btn btn-primary btn-circle" href="{{ url('/orderform/create') }}">2</a>
            <p>Order Form</p>
        </div>
        <!-- <div class="stepwizard-step">
            <a type="button" class="btn btn-success btn-circle" href="{{ url('/payment') }}">3</a>
            <p>Payment</p>
        </div>  -->
    </div>
</div>
		
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">

	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

		
		{{ Form::open(['route' => ['orderform.store'], 'id'=>'myForm', "method" => "POST"]) }}
			
			{{ Form::label('Name') }}<br>
			@if(Auth::check())
				{{ Form::text('name', Auth::user()->name, ['class' => 'form-control']) }}
			@else
				{{ Form::text('name','',['class' => 'form-control']) }}<br>
			@endif

			{{ Form::label('Phone Number') }}<br>
			@if(Auth::check())
				{{ Form::text('contact_phone', Auth::user()->contact_phone, ['class' => 'form-control']) }}
			@else
				{{ Form::text('contact_phone','',['class' => 'form-control']) }}<br>
			@endif

			{{ Form::label('Table') }}<br>
			@if(Auth::check())
				{{ Form::select('table_id', $tables, Auth::user()->table_id, ['class' => 'form-control']) }}
			@else
				{{ Form::text('table_id','',['class' => 'form-control']) }}<br>
			@endif

			{{ Form::label('Number of People') }}<br>
			@if(Auth::check())
				{{ Form::number('number_of_person', Auth::user()->number_of_person, ['class' => 'form-control', 'min' => 1]) }}
			@else
				{{ Form::number('number_of_person','',['class' => 'form-control']) }}<br>
			@endif

			{{ Form::label('Reservation Date and Time') }}<br>
			@if(Auth::check())
				{{ Form::text('reservation_date', Auth::user()->reservation_date, ['class' => 'form-control']) }}
				<p class="help-block">2016-12-12 17:00</p>
			@else
				{{ Form::text('reservation_date', old('reservation_date'), ['class' => 'form-control']) }}<br>
			@endif

 
			{!! Form::submit('Go to Pay', ['class' => 'btn btn-success goToPay']); !!}
			{!! Form::close() !!}
			
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="panel panel-info">
			<div class="panel-heading">Review Order</div>
			<div class="panel-body">
			@if(Session::has('cart.items'))
            @foreach(Session::get('cart.items') as $dish)
				<div class="row">
					<div class="col-sm-3 col-xs-3">
						<img class="img-responsive" src="{{ $dish['photo'] }}" />
					</div>
					<div class="col-sm-6 col-xs-6">
						<div class="col-xs-12">{{ $dish['title'] }}</div>
						<div class="col-xs-12"><small>Quantity:<span>{{ $dish['cart_count'] }}</span></small></div>
					</div>
					<div class="col-sm-3 col-xs-3 text-right">
						<h6>{{ $dish['price'] }}<span> €</span></h6>
					</div>
				</div>
				<div class="form-group"><hr /></div>           
			@endforeach
            @endif 
				
				<div class="form-group">
					<div class="col-xs-12">
						<strong>Subtotal</strong>
						<div class="pull-right cart-total-amount"><span>{{ Session::get('cart.total_amount') }}</span> €</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
@endsection