@extends('main')

@section('content')

<h1>Order Form</h1>

<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <a type="button" class="btn btn-danger btn-circle" href="{{ url('/cart') }}">1</a>
            <p>Cart</p>
        </div>
        <div class="stepwizard-step">
            <a type="button" class="btn btn-primary btn-circle" href="{{ url('/orderform/create') }}">2</a>
            <p>Order Form</p>
        </div>
        <!-- <div class="stepwizard-step">
            <a type="button" class="btn btn-success btn-circle" href="{{ url('/payment') }}">3</a>
            <p>Payment</p>
        </div>  -->
    </div>
</div>




@endsection