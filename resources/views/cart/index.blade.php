@extends('main')

@section('content')

<h1>Cart</h1>

<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <a type="button" class="btn btn-danger btn-circle" href="{{ url('/cart') }}">1</a>
            <p>Cart</p>
        </div>
        <div class="stepwizard-step">
            <a type="button" class="btn btn-primary btn-circle" href="{{ url('/orderform/create') }}">2</a>
            <p>Order Form</p>
        </div>
        <!-- <div class="stepwizard-step">
            <a type="button" class="btn btn-success btn-circle" href="{{ url('/payment') }}">3</a>
            <p>Payment</p>
        </div>  -->
    </div>
</div>


<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="row">
                        <div class="col-xs-6">
                            <h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
                        </div>
                        <div class="col-xs-6">
                            <a href="{{ url('/orderform') }}" type="button" class="btn btn-primary btn-sm btn-block">
                                <span class="glyphicon glyphicon-share-alt"></span> Continue reservation
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body cart-table">
            @if(Session::has('cart.items'))
            @foreach(Session::get('cart.items') as $dish)
                <hr>
                <div class="row">
                    <div class="col-xs-2"><img class="img-responsive" src="{{$dish['photo']}}">
                    </div>
                    <div class="divider"></div>
                    <div class="col-xs-4">
                        <h4 class="product-name"><strong>{{ $dish['title'] }}</strong></h4>
                        <h4><small>{{$dish['description']}}</small></h4>
                    </div>
                    <div class="col-xs-6">
                        <div class="col-xs-6 text-right">
                            <h6><strong>{{ $dish['price'] }}<span class="text-muted">x</span></strong></h6>
                        </div>
                        <div class="col-xs-2">
                            <input type="text" class="form-control input-sm" value="{{ $dish['cart_count'] }}">
                        </div>
                        <div class="col-xs-2">
                            <a href="#" data-dish-id="{{ $dish['id'] }}" class="btn btn-link btn-xs cart-destroy">
                                <span class="glyphicon glyphicon-trash"> </span>
                            </a>
                        </div>
                        <div class="col-xs-2">
                            <h6><strong><span class="text-muted">{{ $dish['cart_amount'] }}</span> €</strong></h6>
                        </div>
                    </div>
                </div>
            @endforeach
            @endif 
            <hr>
            <div class="row">
                       <div class="text-center">
                           <div class="col-xs-9"></div>
                        <div class="col-xs-3">
                        <button type="button" class="btn btn-default btn-sm btn-block">
                            Update cart
                        </button>
                    </div>
                </div>
            </div>
            </div>
            <div class="panel-footer">
                <div class="row text-center">
                    <div class="col-xs-9">
                        <h4 class="text-right">Total <strong><span class="cart-total-amount">{{ Session::get('cart.total_amount') }}</span> €</strong></h4>
                    </div>
                    <div class="col-xs-3">
                        <a  type="button" class="btn btn-success btn-block" href="{{ url('orderform/create') }}">
                            Submit
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection